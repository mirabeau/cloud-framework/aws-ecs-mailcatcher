#!/bin/sh
# Usage: $0 entrypoint
# i.e. ./route-53.sh entrypoint
echo "[DEBUG] Called $# arguments: $@"

# Determine hostedzone and target name from env
if [ -z "TARGETZONE" -o -z "TARGETDNS" ]; then
	echo "No TARGETZONE or TARGETDNS environment variables set -- not setting anything"
	exec $@
fi

# Fetch our IP through introspection
IPADDRESS=`curl -s --connect-timeout 1 http://169.254.169.254/latest/meta-data/local-ipv4`
if [ -z $IPADDRESS ]; then
	echo Could not fetch IP address -- your DNS is failing. Continue without DNS....
	exec "$@"
fi
# Get our region
AZ=`curl -s --connect-timeout 1 http://169.254.169.254/latest/meta-data/placement/availability-zone`
REGION=${AZ::-1}

# Call lambda here
echo Calling lambda to register $TARGETDNS in $TARGETZONE with IP $IPADDRESS
echo Running: aws lambda invoke --function-name Route53Manager --payload '{"DomainName": "'$TARGETDNS'.", "Value": "'$IPADDRESS'", "RecordType": "A", "HostedZoneId": "'$TARGETZONE'", "Action": "UPSERT"}' /dev/stdout --region $REGION
aws lambda invoke --function-name Route53Manager --payload '{"DomainName": "'$TARGETDNS'.", "Value": "'$IPADDRESS'", "RecordType": "A", "HostedZoneId": "'$TARGETZONE'", "Action": "UPSERT"}' /dev/stdout --region $REGION

# Call docker-entrypoint
echo Starting entrypoint: $@
exec "$@"

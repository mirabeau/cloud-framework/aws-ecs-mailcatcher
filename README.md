Role Name
=========

Create Cloudformation stack for Mailcatcher ECS service. AWS resources that will be created are:
 * TaskDefinition with the definition needed to run the mailcatcher
 * ECS Service with the above task definition

Requirements
------------

The role itself does not create an ECS cluster but takes this cluster as a parameter for deployment

Role Variables
--------------

_Role specific_
```
  ecs_mailcatcher_params:
    image           : "{{ account_id }}.dkr.ecr.{{ aws_region }}.amazonaws.com/mailcatcher"
    image_tag       : "latest"

    environment_abbr: "{{ environment_abbr }}"
    stack_name      : "{{ environment_abbr }}-ecs-mailcatcher"
    ecs_cluster_name: "services-ecs"
    desiredcount    : 1

```
_General_
```
  aws_region      : <aws region, eg: eu-west-1>
  owner           : <owner, eg: mirabeau>
  account_name    : <customer name>
  account_id      : <aws account id>
  environment_type: <environment>
  environment_abbr: <environment abbriviation>
```

Dependencies
------------

 * aws-vpc
 * aws-securitygroups
 * ecs-cluster
 * aws-lambda-route53manager

Example Playbook
----------------
Build docker image and push to ECR
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    account_id      : "<your aws account id>"
    aws_region      : "eu-west-1"
    owner           : "myself"
    account_name    : "my-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
  tasks:
    - name: "mailcatcher | docker | build and push"
      include_role:
        name: aws-ecs-mailcatcher
        tasks_from: build-docker
```

Rollout the aws-ecs-mailcatcher files with defaults
```yaml
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    tag_prefix      : "mcf"
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"
    account_abbr    : "dta"
    environment_type: "test"
    environment_abbr: "tst"
    aws_vpc_params:
      network               : "10.10.10.0/24"
      public_subnet_weight  : 1
      private_subnet_weight : 3
      database_subnet_weight: 1
  pre_tasks:
    - name: "Get latest AWS AMI's"
      include_role:
        name: aws-utils
        tasks_from: get_aws_amis
    - name: "Docker build and push"
      include_role:
        name: aws-ecs-mailcatcher
        tasks_from: build-docker
  roles:
    - aws-setup
    - aws-iam
    - aws-vpc
    - env-acl
    - aws-securitygroups
    - aws-lambda
    - aws-ecs-cluster
    - aws-ecs-mailcatcher
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>  
